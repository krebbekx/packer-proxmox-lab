packer {
  required_plugins {
    proxmox = {
      version = " >= 1.1.8"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

source "proxmox-iso" "proxmox-ubuntu-24" {
  proxmox_url = "https://10.0.100.10:8006/api2/json"
  vm_name     = "packer-ubuntu-24"
  iso_url      = "http://iso.repo.lan/ubuntu-24.04-live-server-amd64.iso"
  iso_checksum = "8762f7e74e4d64d72fceb5f70682e6b069932deedb4949c6975d0f0fe0a91be3"
  username         = "${var.pm_user}"
  password         = "${var.pm_pass}"
  token            = "${var.pm_token}"
  node             = "proxmox"
  iso_storage_pool = "local"

  ssh_username           = "${var.ssh_user}"
  ssh_password           = "${var.ssh_pass}"
  ssh_timeout            = "20m"
  ssh_pty                = true
  ssh_handshake_attempts = 20

  boot_wait      = "5s"
  http_directory = "http" # Starts a local http server, serves Preseed file
  boot_command = [
    "<esc><wait><esc><wait><f6><wait><esc><wait>",
    "<bs><bs><bs><bs><bs>",
    "ip=${cidrhost("10.0.100.0/24", 9)}::${cidrhost("10.0.100.0/24", 1)}:${cidrnetmask("10.0.100.0/24")}::::${cidrhost("10.0.100.0/24", 1)} ",
    " autoinstall ds=nocloud-net;s=http://httpd.proxmox.local:80/preseed/ubuntu-24/ ",
    "--- <enter>"
  ]

  insecure_skip_tls_verify = true

  template_name        = "packer-ubuntu-24"
  template_description = "packer generated ubuntu-24.04-server-amd64"
  unmount_iso          = true

  pool       = "packer"
  memory     = 4096
  cores      = 1
  sockets    = 1
  os         = "l26"
  qemu_agent = true
  disks {
    type              = "scsi"
    disk_size         = "30G"
    storage_pool      = "local-lvm"
    storage_pool_type = "lvm"
    format            = "raw"
  }
  network_adapters {
    bridge   = "vmbr0"
    model    = "virtio"
    firewall = true
  }
}

build {
  sources = ["source.proxmox-iso.proxmox-ubuntu-24"]
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "ls /"
    ]
  }
}